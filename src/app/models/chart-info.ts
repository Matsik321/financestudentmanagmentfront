import {ChartSeries} from './chart-series';

export class ChartInfo {
  xAxisData: Array<String>;

  public chartSeriesList: Array<ChartSeries>;
}
