export class FilterTransactionsModel {
  wallet: string;
  type: string;


  constructor(wallet: string, type: string) {
    this.wallet = wallet;
    this.type = type;
  }
}
