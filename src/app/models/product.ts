import {Category} from "./category";
import {Unit} from "./unit";

export class Product {

  productId : number;

  name : string;

  category : Category;

  image : string;

  unit : Unit;
}
