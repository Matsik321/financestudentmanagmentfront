export class AddDeleteResponse {
  result: boolean;
  errorDescription: string;
}
