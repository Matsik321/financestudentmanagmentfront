export class AddTransactionElement {
  elementId: number;
  quantity: number;
  cost: number;
  productId: number;
  type: string;
  transactionId: number;
}
