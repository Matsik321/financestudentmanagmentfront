
export class ChartFilterData {
  types: Array<string>;
  beginYear: number;
  beginMonth: number;
  endMonth: number;
  endYear: number;

  constructor(types: Array<string>, beginYear: number, beginMonth: number, endMonth: number, endYear: number) {
    this.types = types;
    this.beginYear = beginYear;
    this.beginMonth = beginMonth;
    this.endMonth = endMonth;
    this.endYear = endYear;
  }
}
