export class TransactionFormModel {
  transactionId: number;
  name: string;
  cost: number;
  date: Date;
  wallet: number;
  type: string;
}
