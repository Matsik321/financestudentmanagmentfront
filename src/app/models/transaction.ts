import {Dates} from './dates';
import {Wallet} from "./wallet";

export class Transaction {
  transactionId: number;
  name: string;
  cost: number;
  date: Dates;
  wallet: Wallet;
  type: string;

  constructor(transactionId: number, name: string, cost: number, date: Dates, wallet: Wallet, type: string) {
    this.transactionId = transactionId;
    this.name = name;
    this.cost = cost;
    this.date = date;
    this.wallet = wallet;
    this.type = type;
  }
}
