import {Transaction} from './transaction';

export class TransactionTable {

  transactionList: Array<Transaction>;

  numberOfPages: number;

  visiblePages: Array<number>;
}
