import {Product} from './product';

export class TransactionElement {

  elementId: number;

  quantity: number;

  cost: number;

  productId: number;

  product: Product;

  type: string;
}
