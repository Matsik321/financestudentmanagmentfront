export class Category {
  categoryId : number;
  name : string;
  image : string;
  parent : number;

  constructor(categoryId: number) {
    this.categoryId = categoryId;
  }
}
