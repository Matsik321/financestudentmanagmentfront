import { TestBed, inject } from '@angular/core/testing';

import { TypeProviderService } from './type-provider.service';

describe('TypeProviderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TypeProviderService]
    });
  });

  it('should be created', inject([TypeProviderService], (service: TypeProviderService) => {
    expect(service).toBeTruthy();
  }));
});
