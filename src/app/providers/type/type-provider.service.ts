import { Injectable } from '@angular/core';
import {BaseProviderService} from '../base-provider.service';
import {Http, RequestOptions} from '@angular/http';
import {Observable} from "rxjs/Observable";

@Injectable()
export class TypeProviderService extends BaseProviderService {

  url = this.baseUrl +  'type';

  constructor(http: Http) {
    super(http);
  }

  getAvaiblesTypes(): Observable<Array<string>> {
    const baseHeader = this.createAuthorizeHeader();
    if (localStorage.getItem('token')) {
      const options = new RequestOptions({headers: baseHeader});
      return this.http
        .get(this.url, options)
        .map(response => (<Array<string>> response.json()));
    }
  }
}
