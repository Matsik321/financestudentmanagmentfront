import { Injectable } from '@angular/core';
import {Headers, Http} from '@angular/http';
@Injectable()
export class BaseProviderService {

  protected baseUrl = 'http://localhost:8081/';

  constructor(protected http: Http) {
  }

  protected baseHeader = new Headers({'Content-Type': 'application/json'});

  protected createAuthorizeHeader(): Headers {
    const baseHeader = new Headers();
    baseHeader.append('Content-Type', 'application/json');
    baseHeader.append('Authorization', localStorage.getItem('token'));
    return baseHeader;
  }
}
