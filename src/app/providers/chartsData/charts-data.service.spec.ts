import { TestBed, inject } from '@angular/core/testing';

import { ChartsDataService } from './charts-data.service';

describe('ChartsDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChartsDataService]
    });
  });

  it('should be created', inject([ChartsDataService], (service: ChartsDataService) => {
    expect(service).toBeTruthy();
  }));
});
