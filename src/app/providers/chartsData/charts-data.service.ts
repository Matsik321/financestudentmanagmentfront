import { Injectable } from '@angular/core';
import {BaseProviderService} from '../base-provider.service';
import {Http, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {ChartInfo} from '../../models/chart-info';
import {ChartFilterData} from '../../models/chart-filter-data';

@Injectable()
export class ChartsDataService extends BaseProviderService{

  url = this.baseUrl + 'spend/chart';

  constructor(http: Http) {
    super(http);
  }

  getChartData(chartFilterData: ChartFilterData): Observable<ChartInfo> {
    const baseHeader = this.createAuthorizeHeader();
    if (localStorage.getItem('token')) {
      const options = new RequestOptions({headers: baseHeader});
      return this.http
        .post(this.url, chartFilterData, options)
        .map(response => (<ChartInfo> response.json()));
    }
  }
}
