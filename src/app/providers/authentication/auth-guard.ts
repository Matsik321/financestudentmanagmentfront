import {Injectable, OnInit} from "@angular/core";
import {CanActivate, Router} from "@angular/router";

@Injectable()
export class AuthGuard implements CanActivate, OnInit {


  constructor(private router: Router) { }

  ngOnInit() {
  }

  canActivate() {

    if (localStorage.getItem('token')) {
      return true;
    }

     this.router.navigate(['/Account']);
     return false;
  }
}
