import {Inject, Injectable} from '@angular/core';
import {User} from '../../models/user';
import {Http, RequestOptions, Response} from '@angular/http';
// import 'rxjs/add/operator/toPromise';
import {Observable} from 'rxjs/Observable';
import {BaseProviderService} from "../base-provider.service";
@Injectable()
export class AuthService extends BaseProviderService {


  constructor(http: Http) {
    super(http);
  }

  login(user: User): Observable<Response> {
    const body = JSON.stringify(user);
    const options = new RequestOptions({ headers: this.baseHeader });
    return this.http.post(this.baseUrl + 'login', body, options);
  }

  register(user: User): Promise<Response> {
    const body = JSON.stringify(user);
    const options = new RequestOptions({ headers: this.baseHeader });
    return this.http.post(this.baseUrl + 'register', body, options).toPromise();
  }

  saveToken(token: string) {
    localStorage.setItem('token', token);
  }

  logout() {
    localStorage.clear();
  }
}
