import {Injectable, OnInit} from '@angular/core';
import {Http, RequestOptions, Headers, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {TransactionTable} from '../../models/transaction-table';
import {BaseProviderService} from '../base-provider.service';
import {AddDeleteResponse} from '../../models/add-delete-response';
import {TransactionFormModel} from '../../models/transaction-form-model';
import {FilterTransactionsModel} from "../../models/filter-transactions-model";

@Injectable()
export class TransactionService extends BaseProviderService implements OnInit {

  url = this.baseUrl + 'transactions';

  constructor(http: Http) {
    super(http);
  }

  ngOnInit(): void {

  }

  getUserTransactions(page: number, size: number): Observable<TransactionTable> {
    const baseHeader = this.createAuthorizeHeader();
    if (localStorage.getItem('token')) {
      const options = new RequestOptions({headers: baseHeader});
      return this.http
        .get(this.url + '/' + page + '/' + size, options)
        .map(response => (<TransactionTable> response.json()));
    }
  }

  getUserTransactionsFiltered(page: number, size: number, transactionFilter: FilterTransactionsModel): Observable<TransactionTable> {
    const baseHeader = this.createAuthorizeHeader();
    if (localStorage.getItem('token')) {
      const options = new RequestOptions({headers: baseHeader});
      return this.http
        .get(this.url + '/' + page + '/' + size + '/' + transactionFilter.wallet + '/' + transactionFilter.type, options)
        .map(response => (<TransactionTable> response.json()));
    }
  }

  addTransaction(transaction: TransactionFormModel): Observable<AddDeleteResponse> {
    const baseHeader = this.createAuthorizeHeader();
    if (localStorage.getItem('token')) {
      const options = new RequestOptions({headers: baseHeader});
      return this.http
        .post(this.url, transaction , options)
        .map(response => <AddDeleteResponse> response.json());
    }
  }

  deleteTransaction(transactionId: number) {
    const baseHeader = this.createAuthorizeHeader();
    if (localStorage.getItem('token')) {
      const options = new RequestOptions({headers: baseHeader});
      return this.http
        .delete(this.url + '/' + transactionId, options)
        .map(response => <AddDeleteResponse> response.json());
    }
  }
}
