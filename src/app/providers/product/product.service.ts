import { Injectable } from '@angular/core';
import {Http, RequestOptions, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Product} from '../../models/product';
import {BaseProviderService} from "../base-provider.service";

@Injectable()
export class ProductService extends BaseProviderService {

  url = this.baseUrl + 'product';

  constructor(http: Http) {
    super(http);
  }

  getCategoryProducts(categoryId: number): Observable<Product[]> {
    const baseHeader = this.createAuthorizeHeader();
    if (localStorage.getItem('token')) {
      const options = new RequestOptions({headers: baseHeader});
      return this.http
        .get(this.url + '/category/' + categoryId, options)
        .map(response => (<Product[]> response.json()));
    }
  }

  getProduct(productId: number): Observable<Product> {
    const baseHeader = this.createAuthorizeHeader();
    if (localStorage.getItem('token')) {
      const options = new RequestOptions({headers: baseHeader});
      return this.http
        .get(this.url + '/' + productId, options)
        .map(response => (<Product> response.json()));
    }
  }
}
