import {Injectable} from '@angular/core';
import {Http, RequestOptions, Headers} from '@angular/http';
import {Category} from '../../models/category';
import {Observable} from 'rxjs/Observable';
import {BaseProviderService} from '../base-provider.service';

@Injectable()
export class CategoryService extends BaseProviderService {

  url = this.baseUrl + 'category/';

  constructor(http: Http) {
    super(http);
  }

  getCategory(id: number): Observable<Category[]> {
    const baseHeader = this.createAuthorizeHeader();
    if (localStorage.getItem('token')) {
      const options = new RequestOptions({headers: baseHeader});
      return this.http.get(this.url + id, options).map(response => (<Category[]> response.json()));
    }
  }
}
