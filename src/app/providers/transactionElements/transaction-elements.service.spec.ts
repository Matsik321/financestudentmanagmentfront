import { TestBed, inject } from '@angular/core/testing';

import { TransactionElementsService } from './transaction-elements.service';

describe('TransactionElementsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TransactionElementsService]
    });
  });

  it('should be created', inject([TransactionElementsService], (service: TransactionElementsService) => {
    expect(service).toBeTruthy();
  }));
});
