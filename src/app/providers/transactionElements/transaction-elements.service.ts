import { Injectable } from '@angular/core';
import {Http, RequestOptions, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {TransactionElement} from '../../models/transaction-element';
import {BaseProviderService} from '../base-provider.service';
import {AddTransactionElement} from "../../models/add-transaction-element";

@Injectable()
export class TransactionElementsService extends BaseProviderService {

  url = this.baseUrl + 'TransactionElements';

  constructor( http: Http) {
  super(http);
  }

  getTransactionElements( id: number): Observable<TransactionElement[]> {
    const baseHeader = this.createAuthorizeHeader();
    if (localStorage.getItem('token')) {
      const options = new RequestOptions({headers: baseHeader});
      return this.http.get(this.url + '/' + id, options).map(response => (<TransactionElement[]> response.json()));
    }
  }

  deleteTransactionElement( id: number): Promise<any> {
    const baseHeader = this.createAuthorizeHeader();
    if (localStorage.getItem('token')) {
      const options = new RequestOptions({headers: baseHeader});
      return this.http.delete(this.url + '/' + id, options).toPromise();
    }
  }

  addTransactionElement(addTransactionElementModel: AddTransactionElement): Promise<any> {
    const baseHeader = this.createAuthorizeHeader();
    if (localStorage.getItem('token')) {
      const options = new RequestOptions({headers: baseHeader});
      return this.http
        .post(this.url, addTransactionElementModel , options)
        .toPromise();
    }
  }
}
