import { TestBed, inject } from '@angular/core/testing';

import { WalletProviderService } from './wallet-provider.service';

describe('WalletProviderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WalletProviderService]
    });
  });

  it('should be created', inject([WalletProviderService], (service: WalletProviderService) => {
    expect(service).toBeTruthy();
  }));
});
