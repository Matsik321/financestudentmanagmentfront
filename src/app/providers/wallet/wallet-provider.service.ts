import { Injectable } from '@angular/core';
import {Http, RequestOptions} from '@angular/http';
import {BaseProviderService} from '../base-provider.service';
import {Observable} from 'rxjs/Observable';
import {Wallet} from '../../models/wallet';
import {AddDeleteResponse} from "../../models/add-delete-response";

@Injectable()
export class WalletProviderService extends BaseProviderService {

  baseUrl = 'http://localhost:8081/wallets';

  constructor(http: Http) {
    super(http);
  }

  getAvaiblesWallets(): Observable<Array<Wallet>> {
    const baseHeader = this.createAuthorizeHeader();
    if (localStorage.getItem('token')) {
      const options = new RequestOptions({headers: baseHeader});
      return this.http
        .get(this.baseUrl, options)
        .map(response => (<Array<Wallet>> response.json()));
    }
  }

  addNewWallet(wallet: Wallet): Observable<AddDeleteResponse> {
    const baseHeader = this.createAuthorizeHeader();
    if (localStorage.getItem('token')) {
      const options = new RequestOptions({headers: baseHeader});
      return this.http
        .post(this.baseUrl, wallet, options)
        .map(response => (<AddDeleteResponse> response.json()));
    }
  }

}
