import { Component, OnInit } from '@angular/core';
import {Wallet} from '../../models/wallet';
import {WalletProviderService} from '../../providers/wallet/wallet-provider.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.css']
})
export class WalletComponent implements OnInit {

  userWallets: Array<Wallet> = [];

  constructor(private walletProvider: WalletProviderService, private router: Router) { }

  ngOnInit() {
    this.getUserWallets();
  }

  private getUserWallets() {
    this.walletProvider
      .getAvaiblesWallets()
      .subscribe(response =>
      this.userWallets = response);
  }

  isPositive(ballance: number): boolean {
    return ballance > 0;
  }

  navigateToForm(): void {
    this.router.navigate(['AddWallet']);
  }
}
