import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Wallet} from '../../models/wallet';
import {WalletProviderService} from '../../providers/wallet/wallet-provider.service';
import {AddDeleteResponse} from '../../models/add-delete-response';

@Component({
  selector: 'app-add-wallet',
  templateUrl: './add-wallet.component.html',
  styleUrls: ['./add-wallet.component.css']
})
export class AddWalletComponent implements OnInit {

  public addWalletForm: FormGroup;
  errorMessage: string;

  constructor(private _fb: FormBuilder, private walletService: WalletProviderService,
              private router: Router) { }

  ngOnInit() {
    this.addWalletForm = this._fb.group({
      name: ['', [Validators.required, Validators.minLength(4)]],
      balance: [0, [Validators.required]],
      currency: ['', [Validators.required, Validators.maxLength(10) ]]
    });
  }

  addWallet(wallet: Wallet) {
    this.walletService
      .addNewWallet(wallet)
      .subscribe(data => this.proceedResponse(data));
  }

  private proceedResponse(data: AddDeleteResponse): void {
    if (data.result) {
      this.router.navigate(['/Wallets']);
    } else {
      this.errorMessage = data.errorDescription;
    }
  }
}
