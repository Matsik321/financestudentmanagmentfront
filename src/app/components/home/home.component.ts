import {Component, OnInit, QueryList, SimpleChanges, ViewChild, ViewChildren} from '@angular/core';
import {ChartsDataService} from '../../providers/chartsData/charts-data.service';
import {ChartFilterData} from '../../models/chart-filter-data';
import {ChartInfo} from '../../models/chart-info';
import {BaseChartDirective} from 'ng2-charts';
import {IMultiSelectOption, IMultiSelectSettings} from 'angular-2-dropdown-multiselect';
import {FormBuilder, FormGroup} from '@angular/forms';
import {TypeProviderService} from '../../providers/type/type-provider.service';
import {forEach} from '@angular/router/src/utils/collection';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [ChartsDataService]
})
export class HomeComponent implements OnInit {
  public chartRequestForm: FormGroup;
  public lineChartOptions: any = {
    responsive: true
  };
  public yearOptions: Array<any>;
  public monthOptions: Array<any>;
  public typesOptions: IMultiSelectOption[] = [];
  public lineChartLegend = true;
  public lineChartType = 'line';
  private wynik: ChartInfo;
  model: number[];
  public chartTypes: Array<any> = ['line', 'bar'];
  public chartType: string = 'bar';
  singleSelectionSettings: IMultiSelectSettings = {
    checkedStyle: 'fontawesome',
    selectionLimit: 1,
    autoUnselect: true
  };

  @ViewChild(BaseChartDirective) charts: BaseChartDirective;
// lineChart
  public lineChartData: Array<any> = [{data: [0, 0, 0, 0, 0, 0, 0], label: ''}];


  public lineChartLabels: Array<any> = ['January', 'February', 'March'];


  constructor(private chartDataProvider: ChartsDataService, private _fb: FormBuilder,
              private typeProviderService: TypeProviderService) {
    this.chartDataProvider
      .getChartData(new ChartFilterData(['PARTY', 'CLEANING'], 2018, 1, 3, 2018))
      .subscribe(response => {
        this.wynik = response;
        this.lineChartData = this.wynik.chartSeriesList;

        setTimeout(() => {
          this.charts.
          ngOnChanges({} as SimpleChanges);
        }, 100);
      });
  }

  ngOnInit() {
    this.setMonthOptions();
    this.setYearOptions();
    this.setTypeOptions();

    this.chartRequestForm = this._fb.group({
      beginMonth: [1],
      beginYear: [2017],
      endMonth: [1],
      endYear: [2017],
      types: ['PARTY'],
    });
  }

  private setTypeOptions() {
    this.typeProviderService.getAvaiblesTypes().subscribe(response => {
      let array: Array<any> = [];
      for (let i = 0; i < response.length; i++) {
        array.push({id: response[i], name: response[i]});
      }
      this.typesOptions = array;
    });
  }

  private setMonthOptions() {
    this.monthOptions = [
      {id: 1, name: 'January'},
      {id: 2, name: 'February'},
      {id: 3, name: 'March'},
      {id: 4, name: 'April'},
      {id: 5, name: 'May'},
      {id: 6, name: 'June'},
      {id: 7, name: 'July'},
      {id: 8, name: 'August'},
      {id: 9, name: 'September'},
      {id: 10, name: 'October'},
      {id: 11, name: 'November'},
      {id: 12, name: 'December'},
    ];
  }

  private setYearOptions() {
    this.yearOptions = [
      {id: 2016, name: '2016'},
      {id: 2017, name: '2017'},
      {id: 2018, name: '2018'},
    ];
  }

  submit(chartFilterData: ChartFilterData) {
    console.log(chartFilterData);
    this.chartDataProvider.getChartData(chartFilterData).subscribe(response => {
      this.wynik = response;
      this.lineChartData = this.wynik.chartSeriesList;

      setTimeout(() => {
        this.charts.
        ngOnChanges({} as SimpleChanges);
      }, 100);
    });
  }

// public lineChartLabels: Map<number, string> = new Map([
  //   [1, 'January'],
  //   [2, 'February'],
  //   [3, 'March'],
  //   [4, 'April'],
  //   [5, 'May'],
  //   [6, 'June'],
  //   [7, 'July'],
  //   [8, 'August'],
  //   [9, 'September'],
  //   [10, 'October'],
  //   [11, 'November'],
  //   [12, 'December']

}
