import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../providers/authentication/auth.service";
import {Router} from "@angular/router";
import {User} from "../../models/user";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public registerForm: FormGroup;
  errorMessage : string;

  constructor(private authService : AuthService, private _fb: FormBuilder,
              private router: Router) { }

  ngOnInit() {
    this.registerForm = this._fb.group({
      username: ['', [Validators.required, Validators.minLength(5)]],
      password: ['', [Validators.required, Validators.minLength(5)]],
      confirmPassword: ['', [Validators.required, Validators.minLength(5)]]
    },{validator: this.areEqual}
      );
  }

  register(user: User){
    this.authService.register(user).then(data => {
      this.authService.login(user).subscribe(response => {this.authService.saveToken(data.json()['token']);
      this.router.navigate(['/Home']);})
    }, error  => this.saveErrorMessage(error));
  }

  private saveErrorMessage(error: any) {
    this.errorMessage = 'Zły username lub hasło';
  }

  public areEqual(group: FormGroup) {
    let firstPassword = group.controls.password.value;
    let secondPassword = group.controls.confirmPassword.value;
    return firstPassword == secondPassword;
  }

}
