import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Transaction} from '../../models/transaction';
import {TransactionService} from '../../providers/transaction/transaction.service';
import {Router} from '@angular/router';
import {Dates} from '../../models/dates';
import {WalletProviderService} from '../../providers/wallet/wallet-provider.service';
import {Wallet} from '../../models/wallet';
import {AddDeleteResponse} from '../../models/add-delete-response';
import {FilterTransactionsModel} from "../../models/filter-transactions-model";


@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {
  public transactionForm: FormGroup;
  public filterTransactionForm: FormGroup;
  public page = 1;
  public userTransactions: Array<Transaction>;
  public numberOfPages: number;
  public visiblePages: Array<number>;
  public errorMessage: string;
  public wallets: Array<Wallet> = [];
  public types: Array<string> = ['EXPENSE', 'INCOME'];
  private pageSize = 10;
  public leftDotsVisible: boolean;
  public rightDotsVisible: boolean;
  private dateSeparator = '/';
  loadActive = true;
  color = 'primary';
  mode = 'indeterminate';
  value = 50;
  bufferValue = 50;
  public noDataMessage = false;
  public filterActive = false;

  constructor(private _fb: FormBuilder, private transactionService: TransactionService,
              private router: Router, private walletsProvider: WalletProviderService) {
  }

  ngOnInit() {
    this.filterTransactionForm = this._fb.group({
      wallet: ['', [Validators.required, Validators.minLength(5)]],
      type: ['', [Validators.required, Validators.minLength(5)]]
    });
    this.getWallets();
    this.getTransactions();
  }

  private getWallets() {
    this.walletsProvider
      .getAvaiblesWallets()
      .subscribe(response =>
        this.wallets = response);
  }

  nextPage() {
    this.page = this.page + 1;
    this.getTransactions();
  }

  previousPage() {
    this.page = this.page - 1;
    this.getTransactions();
  }

  navigateToPage(selectedPage: number) {
    this.page = selectedPage;
    this.getTransactions();
  }

  getTransactions() {
    this.enableLoading();
    this.transactionService
      .getUserTransactionsFiltered(this.page - 1, this.pageSize,
        new FilterTransactionsModel(this.filterTransactionForm.controls.wallet.value, this.filterTransactionForm.controls.type.value))
      .subscribe(response => {
        this.userTransactions = response.transactionList;
        this.numberOfPages = response.numberOfPages;
        this.updatePages(response.visiblePages);
        this.disableLoading();
        this.showNoDataMessage(this.userTransactions);
      });
  }

  private enableLoading() {
    this.loadActive = true;
  }

  private disableLoading() {
    this.loadActive = false;
  }

  onTransactionClick(id: number) {
    localStorage.setItem('transactionId', id.toString());
    this.router.navigate(['TransactionElements']);
  }

  isActive(selectedPage: number) {
    return this.page === selectedPage;
  }

  private updatePages(visiblePages: Array<number>) {
    this.resetVisiblePages();
    this.setVisiblePages(visiblePages);
    this.handleOverflowPages();
  }

  private handleOverflowPages() {
    this.leftDotsVisible = this.page - 4 > 0;
    this.rightDotsVisible = this.page + 3 < this.numberOfPages;
  }

  private resetVisiblePages() {
    this.visiblePages = [];
  }

  private setVisiblePages(visiblePages: Array<number>) {
    this.visiblePages = visiblePages;
  }

  navigateToForm() {
    this.router.navigate(['AddTransaction']);
  }

  isExpense(transactionType: string): boolean {
    return transactionType === 'EXPENSE';
  }

  isIncome(transactionType: string): boolean {
    return transactionType === 'INCOME';
  }

  parseDate(date: Dates) {
    const year = date.year;
    const month = date.month < 10 ? '0' + date.month : date.month;
    const day = date.day < 10 ? '0' + date.day : date.day;
    return year + this.dateSeparator + month + this.dateSeparator + day;
  }

  deleteTransaction(transactionId: number) {
    this.transactionService
      .deleteTransaction(transactionId)
      .subscribe(response =>
        this.proceedResponse(response)
      );
  }

  private proceedResponse(data: AddDeleteResponse): void {
    if (data.result) {
      this.getTransactions();
    } else {
      this.errorMessage = data.errorDescription;
    }
  }

  private showNoDataMessage(userTransactions: Array<Transaction>) {
    this.noDataMessage = userTransactions.length === 0;
  }

  filterTransactions(filterTransactionsModel: FilterTransactionsModel) {
    this.page = 1;
    console.log(filterTransactionsModel);
    this.transactionService
      .getUserTransactionsFiltered(this.page - 1, this.pageSize, filterTransactionsModel)
      .subscribe(response => {
          this.userTransactions = response.transactionList;
          this.numberOfPages = response.numberOfPages;
          this.updatePages(response.visiblePages);
        }
      );
  }
}
