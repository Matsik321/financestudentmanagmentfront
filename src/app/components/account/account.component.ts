import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../providers/authentication/auth.service";
import {User} from "../../models/user";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  public authenticationForm: FormGroup;
  errorMessage : string;

  constructor(private authService : AuthService, private _fb: FormBuilder,
              private router: Router) { }

  ngOnInit() {
    this.authenticationForm = this._fb.group({
      username: ['', [Validators.required, Validators.minLength(5)]],
      password: ['', [Validators.required, Validators.minLength(5)]]
    });
  }

  login(user: User){
    this.authService.login(user).subscribe(data => {
      this.authService.saveToken(data.json()['token']);
      this.router.navigate(['/Home']);
    }, error  => this.saveErrorMessage(error));
  }

  private saveErrorMessage(error: any) {
    this.errorMessage = 'Wrong username or password';
  }
}
