import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../providers/authentication/auth.service';
import {Router} from '@angular/router';
import {Transaction} from '../../models/transaction';
import {TransactionService} from '../../providers/transaction/transaction.service';
import {WalletProviderService} from '../../providers/wallet/wallet-provider.service';
import {Wallet} from '../../models/wallet';
import {TransactionFormModel} from '../../models/transaction-form-model';
import {AddDeleteResponse} from "../../models/add-delete-response";

@Component({
  selector: 'app-add-transaction',
  templateUrl: './add-transaction.component.html',
  styleUrls: ['./add-transaction.component.css']
})
export class AddTransactionComponent implements OnInit {


  public addTransactionForm: FormGroup;
  public wallets: Array<Wallet> = [];
  public types: Array<String> = ['EXPENSE', 'INCOME'];

  errorMessage: string;

  constructor(private _fb: FormBuilder, private router: Router, private transactionProvider: TransactionService,
              private walletsProvider: WalletProviderService) {
  }

  ngOnInit() {
    this.getWallets();
    this.addTransactionForm = this._fb.group({
      name: ['', [Validators.required, Validators.minLength(5)]],
      date: ['', [Validators.required, Validators.minLength(5)]],
      wallet: ['', [Validators.required]],
      type: ['', [Validators.required]],
      cost: [0],
    });
  }

  addTransaction(transaction: TransactionFormModel) {
    if (this.validateTransaction(transaction)) {
    this.transactionProvider
      .addTransaction(transaction)
      .subscribe(data => {
        this.proceedResponse(data);
      });
    }
  }

  private getWallets() {
    this.walletsProvider
      .getAvaiblesWallets()
      .subscribe(response =>
        this.wallets = response);
  }

  private proceedResponse(data : AddDeleteResponse): void {
    if (data.result) {
      this.router.navigate(['/Transaction']);
    } else {
      this.errorMessage = data.errorDescription;
    }
  }

  private validateTransaction(transaction: TransactionFormModel) {
    if (transaction.cost <= 0) {
      this.errorMessage = 'Value have to be positive';
      return false;
    }
    return true;
  }
}

