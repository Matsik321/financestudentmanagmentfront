import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionElementsComponent } from './transaction-elements.component';

describe('TransactionElementsComponent', () => {
  let component: TransactionElementsComponent;
  let fixture: ComponentFixture<TransactionElementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionElementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionElementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
