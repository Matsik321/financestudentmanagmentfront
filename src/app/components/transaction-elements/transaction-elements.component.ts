import {Component, OnInit} from '@angular/core';
import {TransactionElementsService} from '../../providers/transactionElements/transaction-elements.service';
import {TransactionElement} from '../../models/transaction-element';
import {log} from 'util';

@Component({
  selector: 'app-transaction-elements',
  templateUrl: './transaction-elements.component.html',
  styleUrls: ['./transaction-elements.component.css']
})
export class TransactionElementsComponent implements OnInit {

  transactionElements: Array<TransactionElement>;
  filterWord: string;
  loadActive = true;
  color = 'primary';
  mode = 'indeterminate';
  value = 50;
  bufferValue = 50;
  public noDataMessage = false;

  constructor(private transactionElementService: TransactionElementsService) {
  }

  ngOnInit() {
    this.transactionElementService
      .getTransactionElements(+localStorage.getItem('transactionId'))
      .subscribe(elements => {
          this.transactionElements = elements;
          this.disableLoading();
          this.showNoDataMessage(elements);
        }
      );
  }

  deleteTransactionElement(id: number) {
    this.transactionElementService
      .deleteTransactionElement(id)
      .then(response =>
        this.transactionElements = this.transactionElements
          .filter(item => item.elementId !== id)
      ).catch(reason => log(reason));
  }

  changeFilterWord(inputFilterWord: string) {
    this.filterWord = inputFilterWord;
  }

  private disableLoading() {
    this.loadActive = false;
  }

  private showNoDataMessage(elements: TransactionElement[]) {
    this.noDataMessage = elements.length === 0;
  }
}
