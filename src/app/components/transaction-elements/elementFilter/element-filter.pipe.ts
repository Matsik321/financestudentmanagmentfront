import {Pipe, PipeTransform} from '@angular/core';
import {TransactionElement} from '../../../models/transaction-element';

@Pipe({
  name: 'elementFilter'
})
export class ElementFilterPipe implements PipeTransform {

  transform(items: TransactionElement[], filter: string): any {
    if (!items || !filter) {
      return items;
    }
    return items.filter(item => this.filerItems(item.product.name, filter));
  }

  private oneDifferentLetter(firstWord, secondWord): boolean {
    let countOfDifferent = 0;
    const arrayFromFirstWord = Array.from(firstWord);
    const arrayFromSecondWord = Array.from(secondWord);
    if (arrayFromSecondWord.length > arrayFromFirstWord.length + 1) {
      return false;
    }
    countOfDifferent = this.checkDifferentOfWords(arrayFromFirstWord, arrayFromSecondWord, countOfDifferent);
    if (countOfDifferent > 1) {
      this.deleteFirstWrongLetter(arrayFromFirstWord, arrayFromSecondWord);
      countOfDifferent = this.checkDifferentOfWords(arrayFromFirstWord, arrayFromSecondWord, countOfDifferent);
    }
    return countOfDifferent < 2;
  }

  private checkDifferentOfWords(arrayFromWord1: Array<any>, arrayFromWord2: Array<any>, countOfDifferent: number) {
    for (let i = 0; i < arrayFromWord1.length && i < arrayFromWord2.length; i++) {
      if (arrayFromWord1[i] !== arrayFromWord2[i]) {
        countOfDifferent++;
      }
    }
    return countOfDifferent;
  }

  private deleteFirstWrongLetter(arrayFromWord1: Array<any>, arrayFromWord2: Array<any>) {
    let find: boolean;
    for (let i = 0; i < arrayFromWord1.length && i < arrayFromWord2.length && !find; i++) {
      if (arrayFromWord1[i] !== arrayFromWord2[i]) {
        arrayFromWord2.splice(i, 0, ' ');
        find = true;
      }
    }
  }

  filerItems(item: string, word: string) {

    if (word == null) {
      return true;
    }
    const inputArray = this.processFilterWord(word);
    if (inputArray.length === 0) {
      return true;
    }
    for (let k = 0; k < inputArray.length; k++) {
      if (this.oneDifferentLetter(item, inputArray[k])) {
        return true;
      }
    }
    return false;
  }

  private processFilterWord(word: string) {
    const find = ',';
    const re = new RegExp(find, 'g');
    const wordsInputed = word.replace(re, ' ');

    let inputArray = wordsInputed.split(' ');
    const cleanedArray = [];
    for (let i = 0; i < inputArray.length; i++) {
      inputArray[i] = inputArray[i].trim();
      if (inputArray[i] !== '') {
        cleanedArray.push(inputArray[i]);
      }
    }
    inputArray = cleanedArray;
    return inputArray;
  }
}
