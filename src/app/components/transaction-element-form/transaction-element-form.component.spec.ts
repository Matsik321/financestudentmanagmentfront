import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionElementFormComponent } from './transaction-element-form.component';

describe('TransactionElementFormComponent', () => {
  let component: TransactionElementFormComponent;
  let fixture: ComponentFixture<TransactionElementFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionElementFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionElementFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
