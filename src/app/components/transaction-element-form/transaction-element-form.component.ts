import {Component, OnInit} from '@angular/core';
import {Category} from '../../models/category';
import {CategoryService} from '../../providers/category/category.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Product} from '../../models/product';
import {ProductService} from '../../providers/product/product.service';
import {TypeProviderService} from '../../providers/type/type-provider.service';
import {TransactionElementsService} from "../../providers/transactionElements/transaction-elements.service";
import {Router} from "@angular/router";
import {AddTransactionElement} from "../../models/add-transaction-element";

@Component({
  selector: 'app-transaction-element-form',
  templateUrl: './transaction-element-form.component.html',
  styleUrls: ['./transaction-element-form.component.css'],
  providers: [CategoryService, ProductService]
})
export class TransactionElementFormComponent implements OnInit {

  categoryList: Array<Category> = [];
  categories: Array<number> = [];
  public addProductForm: FormGroup;
  public productList: Array<Product> = [];
  typeList: Array<string> = [];
  errorMessage = '';

  constructor(private _fb: FormBuilder, private categoryService: CategoryService,
              private productService: ProductService, private typeService: TypeProviderService,
              private transactionElementsService: TransactionElementsService, private router: Router) {
  }

  ngOnInit() {
    this.addProductForm = this._fb.group({
      productId: ['', [Validators.required, Validators.minLength(5)]],
      quantity: ['', [Validators.required, Validators.min(0)]],
      cost: ['', [Validators.required, Validators.min(0)]],
      type: ['', [Validators.required]],
    });

    this.onCategoryClick(new Category(1));
    this.typeService
      .getAvaiblesTypes()
      .subscribe(response => {
        console.log(response);
        this.typeList = response;
      });
  }

  onCategoryClick(category: Category): void {
    this.categoryService
      .getCategory(category.categoryId)
      .subscribe(response => {
        this.processResponse(response, category);
      });
  }

  processResponse(response: Category[], category: Category) {
    if (this.isEmpty(response)) {
      this.getCategoryProducts(category.categoryId);
    }
    this.categoryList = response;
    this.addToCategoryStack(category);
  }

  getCategoryProducts(categoryId: number) {
    this.productService
      .getCategoryProducts(categoryId)
      .subscribe(response => this.productList = response);
  }

  isEmpty(categoryList: Array<Category>): boolean {
    return categoryList.length === 0;
  }

  onBackClicked() {
    this.categoryService
      .getCategory(this.categories.pop())
      .subscribe(response => this.categoryList = response);
    this.productList = [];
  }

  addToCategoryStack(category: Category): void {
    if (category.parent) {
      this.categories.push(category.parent);
    }
  }

  addElement(element: AddTransactionElement) {
    element.transactionId = +localStorage.getItem('transactionId');
    console.log(element);
    this.transactionElementsService
      .addTransactionElement(element)
      .then(response =>
        this.router.navigate(['/TransactionElements'])
      );
  }
}
