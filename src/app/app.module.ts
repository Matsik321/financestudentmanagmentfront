import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';

import {RouterModule, Routes} from '@angular/router';
import {AppComponent} from './app.component';
import {MainComponent} from './components/main/main.component';
import {AccountComponent} from './components/account/account.component';
import {HomeComponent} from './components/home/home.component';
import {AuthService} from './providers/authentication/auth.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthGuard} from './providers/authentication/auth-guard';
import {TransactionsComponent} from './components/transactions/transactions.component';
import {TransactionElementsComponent} from './components/transaction-elements/transaction-elements.component';
import {TransactionElementsService} from './providers/transactionElements/transaction-elements.service';
import {TransactionElementFormComponent} from './components/transaction-element-form/transaction-element-form.component';
import {ComboBoxModule} from 'ng2-combobox';
import {RegisterComponent} from './components/register/register.component';
import {ElementFilterPipe} from './components/transaction-elements/elementFilter/element-filter.pipe';
import {MatPaginatorModule} from '@angular/material';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import {ChartsModule} from 'ng2-charts';
import {MultiselectDropdownModule} from 'angular-2-dropdown-multiselect';
import {TypeProviderService} from './providers/type/type-provider.service';
import {MatSelectModule} from '@angular/material';
import {WalletComponent} from './components/wallet/wallet.component';
import {WalletProviderService} from "./providers/wallet/wallet-provider.service";
import {AddTransactionComponent} from "./components/add-transaction/add-transaction.component";
import {TransactionService} from "./providers/transaction/transaction.service";
import {AddWalletComponent} from "./components/add-wallet/add-wallet.component";
import {MatProgressBarModule} from '@angular/material';
const appRoutes: Routes = [
  {path: '', component: MainComponent, canActivate: [AuthGuard]},
  {path: 'Account', component: AccountComponent},
  {path: 'Register', component: RegisterComponent},
  {path: 'Home', component: HomeComponent, canActivate: [AuthGuard]},
  {path: 'Transaction', component: TransactionsComponent, canActivate: [AuthGuard]},
  {path: 'TransactionElements', component: TransactionElementsComponent, canActivate: [AuthGuard]},
  {path: 'TransactionElementsForm', component: TransactionElementFormComponent, canActivate: [AuthGuard]},
  {path: 'Wallets', component: WalletComponent, canActivate: [AuthGuard]},
  {path: 'AddTransaction', component: AddTransactionComponent, canActivate: [AuthGuard]},
  {path: 'AddWallet', component: AddWalletComponent, canActivate: [AuthGuard]},
];


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    AccountComponent,
    HomeComponent,
    TransactionsComponent,
    TransactionElementsComponent,
    TransactionElementFormComponent,
    RegisterComponent,
    ElementFilterPipe,
    WalletComponent,
    AddTransactionComponent,
    AddWalletComponent
  ],
  imports: [
    HttpModule,
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    ReactiveFormsModule,
    ComboBoxModule,
    MatPaginatorModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    ChartsModule,
    MultiselectDropdownModule,
    FormsModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatProgressBarModule
  ],
  providers: [
    AuthService,
    AuthGuard,
    TransactionElementsService,
    TypeProviderService,
    WalletProviderService,
    TransactionService
  ],

  bootstrap: [AppComponent]
})
export class AppModule {
}
