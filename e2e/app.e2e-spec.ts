import { FinanceStudentManagementPage } from './app.po';

describe('finance-student-management App', () => {
  let page: FinanceStudentManagementPage;

  beforeEach(() => {
    page = new FinanceStudentManagementPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
